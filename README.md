# AUS_WA_CoralBay
Western Australia, mid-coast region at the southern end of the Ningaloo Marine Reserve (and reef), filtering Leeuwin Current coastal southward flows

## Roadmap
- visual: map swell, wind, sst
- quantify: 
+comparative years, 2021-22 La-Nina-Neutral evolution, personal observations in the SW of high-salinity inshore 
+low wind events (<10 knots)

## Files
- 3mile.Rmd - investigation, Gnaraloo camp
- CorBay.Rmd - investigation, closest buoy to Gnaraloo camp
- files - nc list for data scrape, prac - practise/test before long-list scrape
- /nc - alternate data scrape form .nc files

## Description
INFO
IMOS Satellite Remote Sensing Facility (SRS, ie; IMOS_SRS-Surface-Waves_MW_CRYOSAT-2_FV02_024S-113E-DM00.nc)
- SRS Facility, supporting calibration & streaming of satellite data in the Southern hemisphere
- access to high-quality data products through 6 Sub-Facilities that make-up Satellite Remote Sensing.
- Radar altimeters provide measurements of sea surface height with unmatched precision over the global ocean, revealing regional variations in water density and giving one of the most reliable measurements of the long-term change in ocean volume, sensors measure surface roughness, wind direction, and surface salinity.
https://imos.org.au/facilities/srs

- TOPEX/Poseidon (1992-2006) was joined in 2001, and later replaced by 
- Jason-1 (2001-2013) , which continued to build the database.
- Jason-2 satellite (OSTM/Jason-2), launched in June 2008, took ocean surface topography measurements
- Jason-3 launched on January 17, 2016 continue U.S.-European satellite measurements of global ocean height changes
* Topex/Poseidon & Jason missions are in orbits that take 10 days to finish re-sampling a criss-crossing pattern of ascending (/) and descending (\) passes sampling ground tracks which are about 250km apart at mid-latitudes. 
- SARAL (Satellite with ARgos and ALtiKa) is a cooperative altimetry technology mission of Indian Space Research Organisation (ISRO) and Centre National d'Études Spatiales (CNES). SARAL performs altimetric measurements designed to study ocean circulation and sea surface elevation.
- Sentinel 3-B sea surface topography, sea and land surface temperature, and ocean and land surface colour
- CryoSat is an ESA programme to monitor variations in the extent and thickness of polar ice 
- HY-2 monitor the dynamic ocean environment with radar sensors to measure sea surface wind field, sea surface height and sea surface
- ERS-1 European Space Agency's first Earth-observing satellite programme using a polar orbit, radar altimeter, ozone
- Envisat polar-orbiting Earth-observation satellite that provides measurements of the atmosphere, ocean, land and ice
* ERS and Envisat ground tracks are about 160km apart at mid-latitudes, but are re-sampled only every 35 days
https://sealevel.jpl.nasa.gov/ocean-observation/why-study-the-ocean/overview/


Topex is the first dual-frequency radar altimeter (Ku band 13.6GHz and C band 5.3GHz) = wind speed retrieval WITH rain
C-band altimetry backscatter coefficient, low subsection statistical relationship of C band & Ku band WITH specular scattering theory
C band backscatter coefficient = 12 dB to 26 dB & if C band backscatter coefficient is below 12 dB. The adjusted method can improve sea surface wind speed accuracy under rainfall conditions.
https://medcraveonline.com/AAOAJ/adjustment-of-the-dual-frequency-altimeter-low-subsection-backscatter-coefficient-statistical-modelnbsp.html

Scatterometers are active remote sensing instruments for deriving wind direction and speed from the roughness of the sea. They are used by low Earth orbiting satellites and act like radars: they transmit electromagnetic pulses and detect the backscattered signals. Spaceborne wind scatterometry is an indirect method of measurement. It has become an increasingly important tool for monitoring climate, forecasting (marine) weather and studying the atmosphere-ocean interactions

SEE LINK!!
http://www.eumetrain.org/data/4/438/navmenu.php?tab=2&page=3.0.0

## Source / Resources
Waverider_buoys_Observations_-_Australia_-_delayed_(National_Wave_Archive)_source_files' # URL to data

DOT-WA

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Contributing
I am 'cleaning up' all my projects/files towards publishing a paper, allowing me to apply for further studies and grants etc.. all help is appreciated. 

## Authors and acknowledgment
Lets work together!

## License
Private invitation collaborations towards publishing, but always open to sharing techniques/code.

## Project status
Ongoing
